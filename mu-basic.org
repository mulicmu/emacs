* Some Basic Settings
  :PROPERTIES:
  :CUSTOM_ID: basic
  :END:

#+NAME: basic
#+BEGIN_SRC emacs-lisp
  (server-start)
  ;; name
  (setq frame-title-format "mu@emacs - %f")
  ;; auto revert buffer globally
  (global-auto-revert-mode t)
  ;; set TAB and indention
  (setq-default tab-width 4)
  (setq-default indent-tabs-mode nil)
  ;; y or n is suffice for a yes or no question
  (fset 'yes-or-no-p 'y-or-n-p)
  ;; always add new line to the end of a file
  (setq require-final-newline t)
  ;; add no new lines when "arrow-down key" at the end of a buffer
  (setq next-line-add-newlines nil)
  ;; prevent the annoying beep on errors
  (setq ring-bell-function 'ignore)
  ;; remove trailing whitespaces before save
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  ;; enable to support navigate in camelCase words
  (global-subword-mode t)
  ;; hide startup splash screen
  (setq inhibit-startup-screen t)

  ;; shell-mode settings
  ;(setq explicit-shell-file-name "/bin/bash")
  ;(setq shell-file-name "/bin/bash")
  ;; always insert at the bottom
  (setq comint-scroll-to-bottom-on-input t)
  ;; no duplicates in command history
  (setq comint-input-ignoredups t)
  ;; what to run when i press enter on a line above the current prompt
  (setq comint-get-old-input (lambda () ""))
  ;; max shell history size
  (setq comint-input-ring-size 1000)
  ;; show all in emacs interactive output
  (setenv "PAGER" "cat")
  ;; set lang to enable Chinese display in shell-mode
  (setenv "LANG" "en_US.UTF-8")

  ;; set text-mode as the default major mode, instead of fundamental-mode
  ;; The first of the two lines in parentheses tells Emacs to turn on Text mode
  ;; when you find a file, unless that file should go into some other mode, such
  ;; as C mode.
  (setq-default major-mode 'text-mode)

  ;; use icomplete in minibuffer
  ;(icomplete-mode t)

  ;; delete the selection with a keypress
  (delete-selection-mode t)
  ;; no cursor
  (blink-cursor-mode -1)

;; disable backup
(setq backup-inhibited t)
;; disable auto save
(setq auto-save-default nil)
;; auto-fill paragraphs when insert space or return
(setq-default auto-fill-function 'do-auto-fill)
;; default 80 chars a line
(setq-default fill-column 80)
(global-auto-revert-mode t)
#+END_SRC


* ido-mode
  use =ido= to switch buffers and open files, the former is also binded into =C-j=
#+NAME: basic
#+BEGIN_SRC emacs-lisp
(require 'ido)                      ; ido is part of emacs
(ido-mode t)                        ; for both buffers and files
(setq
 ido-ignore-buffers               ; ignore these guys
 '("\\` " "^\*Mess" "^\*Back" ".*Completion" "^\*Ido")
; ido-work-directory-list '("~/" "~/Desktop" "~/Documents")
 ido-case-fold  t               ; be case-insensitive
 ido-use-filename-at-point nil    ; don't use filename at point (annoying)
 ido-use-url-at-point nil         ;  don't use url at point (annoying)
 ido-enable-flex-matching t       ; be flexible
 ido-max-prospects 4              ; don't spam my minibuffer
 ido-confirm-unique-completion nil) ; wait for RET, even with unique :completion

(setq ido-file-extensions-order '(".org" ".tex" ".m" ".cpp" ".cc"))

(add-to-list 'completion-ignored-extensions ".gz")
(setq ido-ignore-extensions t)

(global-set-key (kbd "C-x b") 'ido-switch-buffer)
(global-set-key (kbd "C-j") 'ido-switch-buffer)
(add-hook 'org-mode-hook (lambda ()
                          (local-set-key "\C-j" 'ido-switch-buffer)))
#+END_SRC
