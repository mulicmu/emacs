;;;;;;;;;;;;; for latex ;;;;;;;;;;;;;;;;

(add-to-list 'load-path (expand-file-name
                         "/opt/local/share/emacs/site-lisp/"))


;; sudo gedit /usr/share/texmf/web2c/texmf.cnf
;; change file_line_error_style t
;; sudo update-texmf

;(add-to-list 'load-path (expand-file-name "~/.emacs.d/elpa/auctex-11.86"))
;(load "auctex-autoloads.el" nil t t)

;(add-to-list 'load-path (expand-file-name "~/.emacs.d/plugins/auctex-11.87"))
;(load "auto-loads.el" nil t t)

					; (load "preview-latex.el" nil t t)
(load "auctex.el" nil t t)

;(add-to-list 'load-path (expand-file-name "/usr/share/emacs23/site-lisp/"))
;(load "auctex.el" nil t t)


(load "preview-latex.el" nil t t)
(setq preview-scale-function 1.5)

;;;;;;;;; synctex
(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-start-server t)
(setq TeX-source-correlate-method 'synctex )


(setq TeX-view-program-list
      '(("sumatra" "sumatra.exe %o")
        ("preview.app" "open -a Preview.app %o")
        ("Gsview" "gsview32.exe %o")
        ("Okular" "okular --unique %o")
        ("Evince" "evince %o")
        ("Firefox" "firefox %o")))

(cond
 ((eq system-type 'windows-nt)
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              (setq TeX-view-program-selection '((output-pdf "sumatra"))))))

 ((eq system-type 'gnu/linux)
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              (setq TeX-view-program-selection '((output-pdf "Okular"))))))
 ((eq system-type 'darwin)
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              (setq TeX-view-program-selection '((output-pdf "preview.app"))))))
)


; TeX-view-predicate-list TeX-view-program-list
;(setq TeX-view-program-selection "sumatra")

;(setq TeX-math-close-double-dollar t)
;(setq TeX-electric-sub-and-superscript t)
;(setq TeX-insert-dollar nil)
(setq TeX-newline-function 'reindent-then-newline-and-indent)
;(setq reftex-plug-into-auctex t)

(setq LaTeX-math-list '(
			(?/ "frac")
			(?= "nonumber")
			(?. "ldots")
			))

(add-hook 'LaTeX-mode-hook
	  (lambda()
        (yas-minor-mode t)
	    (TeX-PDF-mode t)
	    (LaTeX-math-mode)
	    (setq TeX-save-query  nil)
	    (setq TeX-show-compilation nil)
	    (turn-on-reftex)
	    (outline-minor-mode)
	    (setq TeX-auto-untabify t     ; remove all tabs before saving
                  ;; TeX-engine 'tex       ; use xelatex default tex or xetex
                  ;; TeX-show-compilation t ; display compilation windows
		  )
	    (add-to-list 'TeX-command-list '("Xelatex" "%`xelatex%(mode)%' %t -synctex=-1" TeX-run-TeX nil t))
	    (define-key LaTeX-mode-map "\M-j" 'TeX-insert-dollar)
	    (define-key LaTeX-mode-map "\C-i" 'TeX-insert-dollar)
            (define-key LaTeX-mode-map (kbd "<f9>") 'font-lock-fontify-buffer)
	    (define-key LaTeX-mode-map  (kbd "C-j") 'ido-switch-buffer)
            (define-key LaTeX-mode-map (kbd "<f5>") 'TeX-command-master)
            (define-key LaTeX-mode-map "\C-c\C-v"  'okular-jump-to-line)
            (flyspell-mode t)
;            (setq font-lock-mode-major-mode nil)

            ;(font-lock-fontify-buffer)
;	    (define-key latex-math-mode-map "\M-i"
;	      'TeX-insert-sub-or-superscript)
;	    (define-key latex-mode-map "\M-j"
;	      'TeX-insert-backslash nil)
;	    (define-key latex-mode-map "\C-\S-h"
                                        ;	      'TeX-insert-dollar)
;	    (turn-on-auto-fill)
;	    (define-key latex-mode-map (kbd "TAB") 'TeX-complete-symbol)
	    ))
;xelatex

(setq outline-minor-mode-prefix "\C-c\C-o")


; (require 'okular-search)
;; (cond
;;  ((eq system-type 'windows-nt)
;;   (add-hook 'latex-mode-hook
;;             (lambda ()
;;               (setq TeX-view-program-selection '((output-pdf "SumatraPDF")
;;                                                  (output-dvi "Yap"))))))

;;  ((eq system-type 'gnu/linux)
;;   (add-hook 'latex-mode-hook
;;             (lambda ()
;; 	      (setq TeX-view-program-selection '((output-pdf "mupdf")
;;                                                  (output-dvi "Okular")))))))

(setq reftex-toc-split-windows-horizontally nil)
; (setq reftex-toc-split-windows-fraction 0.2)

; (setq TeX-view-program-selection '((output-pdf "mupdf")))
;;;; F8 for jump
; (require 'sumatra-forward)

;;;;;;;; beamer ;;;;;;;;;;;

;; allow for export=>beamer by placing

;; #+latex_CLASS: beamer in org files
;(unless (boundp 'org-export-latex-classes)
;  (setq org-export-latex-classes nil))

(eval-after-load "org-export-latex"
'(add-to-list 'org-export-latex-classes
  ;; beamer class, for presentations
  '("beamer"
     "\\documentclass[11pt]{beamer}\n"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\begin{frame}[fragile]\\frametitle{%s}"
       "\\end{frame}"
       "\\begin{frame}[fragile]\\frametitle{%s}"
       "\\end{frame}")
     ("\\begin{block}{%s\}"
      "\\end{block}"
      "\\begin{block}{%s\}"
      "\\end{block}"
      ))))
(eval-after-load "org-export-latex"
'(add-to-list 'org-export-latex-classes
    '("mart"
     "\\documentclass[12pt]{article}"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))

  )
)


(setq bibtex-align-at-equal-sign t)
(setq bibtex-autokey-name-case-convert (quote capitalize))
(setq bibtex-autokey-name-length 3)
(setq bibtex-autokey-names (quote infty))
(setq bibtex-autokey-titleword-length 0)
(setq bibtex-autokey-titlewords 0)
(setq bibtex-entry-format (quote (opts-or-alts
				   required-fields
				   numerical-fields
				   page-dashes
				   realign
				   delimiters
				   unify-case)))

(eval-after-load "tex"
    '(progn
(TeX-add-style-hook
 "latex"
 (lambda ()
   (LaTeX-add-environments
    '("theorem" LaTeX-env-label)
    '("lemma" LaTeX-env-label)
    '("corollary" LaTeX-env-label)
    "proof"
    )))))

;; (add-hook 'LaTeX-mode-hook
;;         (lambda ()
;;                 (setq flymake-allowed-file-name-masks
;;                     (delete '("[0-9]+\\.tex\\'"
;;                          flymake-master-tex-init
;;                          flymake-master-cleanup)
;;                          flymake-allowed-file-name-masks));NO multipart files
;;                 (defun flymake-get-tex-args (file-name)
;;                         (list "pdflatex" (list "-file-line-error" "-draftmode"
;;                         "-interaction=nonstopmode" file-name)))
;;                 (unless (eq buffer-file-name nil) (flymake-mode 1))
;;                 ;; (local-set-key [f2] 'flymake-goto-prev-error)
;;                 ;; (local-set-key [f3] 'flymake-goto-next-error)
;;         ))
(setq LaTeX-command-style '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)")))
